<?php

namespace App\Listeners;

use App\Transaction;
use App\Events\UserDepositCalculated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CommitCalculatedDepositTransactionDatabase
{
    /**
     * Handle the event.
     *
     * @param  UserDepositCalculated  $event
     * @return void
     */
    public function handle(UserDepositCalculated $event)
    {
        Transaction::create([
            'user_id' => $event->deposit->user_id,
            'deposit_id' => $event->deposit->id,
            'wallet_id' => $event->deposit->wallet_id,
            'type' => Transaction::STATUS_DEPOSIT_CLOSED,
            'amount' => $event->deposit->invested,
        ]);
    }
}
