<?php

/** @noinspection ALL */

namespace App\Console\Commands;

use App\User;
use App\Wallet;
use App\Deposit;
use App\Transaction;
use App\Jobs\UserDeposit;
use Illuminate\Console\Command;
use App\Events\UserAccrueDeposit;
use Illuminate\Support\Facades\Log;
use App\Events\UserDepositCalculated;

class AccrueDeposit extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'accrue:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculating deposit';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->info('Running and calculating ...');

        $deposits = Deposit::whereStatus(Deposit::STATUS_OPEN)->with('wallet')->get();

        $deposits->each(function (Deposit $deposit) {

            $deposit->wallet()
                ->increment('balance', $deposit->invested * Deposit::PERCENT);

            $deposit->times += 1;


            if ($deposit->times == 10) {
                $deposit->status = Deposit::STATUS_CLOSE;

                event(new UserDepositCalculated($deposit));
            }

            $deposit->save();
        });
    }
}
