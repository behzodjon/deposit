<?php

namespace App\Events;


class UserDepositCalculated
{

    public $deposit;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($deposit)
    {
        return $this->deposit = $deposit;
    }
}
