<?php

namespace App\Events;


class UserFilledWallet
{

    public $userWallet;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($userWallet)
    {
        $this->userWallet = $userWallet;
    }
}
