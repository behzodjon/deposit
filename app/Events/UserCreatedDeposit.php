<?php

namespace App\Events;


class UserCreatedDeposit
{

    public $userDeposit;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($userDeposit)
    {
        return $this->userDeposit=$userDeposit;
    }

}
