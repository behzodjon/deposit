<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\User;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function index(User $user)
    {
        $transactions = Transaction::where('user_id',$user->id)->get();

        return view('transactions.index', compact('user','transactions'));
    }
}
