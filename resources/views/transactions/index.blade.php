
@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">Dashboard</div>

        <div class="card-body">
        </div> 

          <table class="table table-hover">
            <thead>
              <tr>
                <th scope="col">Id</th>
                <th scope="col">Type</th>
                <th scope="col">Amount</th>
                <th scope="col">Date</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($transactions as $transaction )
              <tr>
                <td>{{$transaction->id}}</td>
                <td>{{$transaction->type}}</td>
                <td>{{$transaction->amount}}</td>
                <td>{{$transaction->created_at}}</td>
              </tr>    
              @endforeach
              
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection