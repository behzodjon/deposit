@csrf
<input type="hidden" value="{{$wallet->id}}" name="wallet_id">    
<div class="form-group">
      <label>Balance To Deposit</label>
      <input min="10" max="100" type="number" class="form-control" name="invested" placeholder="Please enter deposit from 10 to 100">
    </div>
    
    <button type="submit" class="btn btn-primary">Submit</button>
  