
@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">Dashboard</div>

        <div class="card-body">
        </div> 

          <table class="table table-hover">
            <thead>
              <tr>
                <th scope="col">Id</th>
                <th scope="col">Invested</th>
                <th scope="col">Percent</th>
                <th scope="col">Calculating times</th>
                <th scope="col">Status</th>
                <th scope="col">Date</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($deposits as $deposit )
              <tr>
                <td>{{$deposit->id}}</td>
                <td>{{$deposit->invested}}</td>
                <td>{{\App\Deposit::PERCENT*100}} %</td>
                <td>{{$deposit->times}}</td>
                <td>{{$deposit->status}}</td>
                <td>{{$deposit->created_at}}</td>
              </tr>    
              @endforeach
              
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection